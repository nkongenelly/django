=====
nellynnscrumy
=====

nellynnscrumy is a packaged django app that reads welcome to django

Install the app and go to /nellynnscrumy to see the content of the app

Detailed documentation is in the "docs" directory.

Quick start
-----------

1. Add "nellynnscrumy" to your INSTALLED_APPS setting like this::

    INSTALLED_APPS = [
        ...
        'nellynnscrumy',
    ]

2. Include the nellynnscrumy URLconf in your project urls.py like this::

    path('nellynnscrumy/', include('nellynnscrumy.urls')),

3. Run ``python manage.py migrate`` to create the nellynnscrumy models.

4. Visit http://127.0.0.1:8000/nellynnscrumy to view.