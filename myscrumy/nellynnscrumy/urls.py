from django.urls import path

from . import  views
app_name = 'nellynnscrumy'
urlpatterns = [
    path('', views.index),
    path('movegoal/<int:goal_id>', views.move_goal)
]
