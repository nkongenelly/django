from django.db import models
from django.contrib.auth.models import User


# Create your models here.

class GoalStatus(models.Model):
    # Stores all possible status goals
    status_name = models.CharField(max_length=255)

    def __str__(self):
        return self.status_name


class ScrumyGoals(models.Model):
    # Keeps record of each goals
    goal_name = models.CharField(max_length=255)
    goal_id = models.IntegerField()
    created_by = models.CharField(max_length=255)
    moved_by = models.CharField(max_length=255)
    owner = models.CharField(max_length=255)
    goal_status = models.ForeignKey(GoalStatus, on_delete=models.PROTECT)
    user = models.ForeignKey(User, related_name='admin_user',on_delete = models.PROTECT)

    def __str__(self):
        return self.goal_name


class ScrumyHistory(models.Model):
    # Stores information about activities/actions carried out on a goal
    moved_by = models.IntegerField()
    created_by = models.IntegerField()
    moved_from = models.IntegerField()
    moved_to = models.IntegerField()
    time_of_action = models.DateTimeField(auto_now_add=True)
    goal = models.ForeignKey(ScrumyGoals, on_delete=models.PROTECT)

    def __str__(self):
        return self.created_by


